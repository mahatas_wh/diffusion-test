package org.example;

import com.pushtechnology.diffusion.client.Diffusion;
import com.pushtechnology.diffusion.client.features.Topics;
import com.pushtechnology.diffusion.client.session.Session;
import com.pushtechnology.diffusion.datatype.json.JSON;

public class Main {

    public static void main(String[] args) {
        new Main().run();
    }

    private void run() {

        final String currentDiffusionUrl = "ws://scoreboards-push-internal.sports.aws-eu-west-1.prod.williamhill.plc:80";
        final String newDiffusionUrl = "wss://sb-push-internal-slave.sb-prod-eks-int.sports.aws-eu-west-1.prod.williamhill.plc/";

        final String serverUrl = currentDiffusionUrl;
        final String topicPattern = "scoreboards/v1/%s";
        final String eventId = "OB_EV27716943";

        try(Session session = Diffusion.sessions().noCredentials().open(serverUrl)) {
            session.addListener(new Session.Listener.Default());

            final Topics topics = session.feature(Topics.class);

            final String topicSelector = String.format(topicPattern, eventId);
            topics.addStream(topicSelector, JSON.class, new Topics.ValueStream.Default<>());
            topics.subscribe(topicSelector, new Topics.CompletionCallback.Default());

            runForever();
        }

    }

    private void runForever() {
        synchronized(this) {
            try {
                wait();
            } catch (InterruptedException e) {
                throw new RuntimeException(e);
            }
        }
    }
}